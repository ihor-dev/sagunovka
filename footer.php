    <? if(is_front_page()):?>
        <section class="l-bg">        
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="fs1 center"><?the_field('h_links_title');?></p>
                        <span class="line"></span>
                        <p class="fs3 center text-grey"><?the_field('h_links_subtitle');?></p>
                    </div>
                </div>
                <div class="row" id="organy_vlady">
                    <div class="col-sm-12 center">
                        <? $ID = get_id_by_slug('main-page'); ?>
                        
                        <? if(have_rows('h_links', $ID)):?>
                            <? $i=0; while(have_rows('h_links', $ID)): the_row(); $i++;?>
                                <? if($i==5){
                                    $animclass="slide-up";
                                }else{
                                    if($i%2){
                                        $animclass = "slide-right";
                                    }else{
                                        $animclass = "slide-left";
                                    }
                                }?>
                                <a href="<?the_sub_field('url');?>" class="i-b  animation-element <?=$animclass;?>">
                                    <img src="<?the_sub_field('img');?>" alt="">
                                    <p class="fs2 center text-orange"><?the_sub_field('title');?></p>
                                </a>
                            <? endwhile;?>
                        <? endif;?>

                    </div>
                </div>
            </div>
        </section>
    <? else: ?>
        <div class="container-inner">
            <div class="row small-icons" id="organy_vlady">                
                <div class="col-sm-12 center">
                    <? $ID = get_id_by_slug('main-page'); ?>
                    
                    <? if(have_rows('h_links', $ID)):?>
                        <? while(have_rows('h_links', $ID)): the_row();?>
                            <a href="<?the_sub_field('url');?>" class="i-b center">
                                <img src="<?the_sub_field('img');?>" alt="">
                                <p class="fs3 text-orange center"><?the_sub_field('title');?></p>
                            </a>
                        <? endwhile;?>
                    <? endif;?>

                </div>
            </div>
        </div>
    <? endif;?>        
    </section>
    <section class="y-bg" id="form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="fs1 center">Зв'язатись з нами</p>
                    <span class="line"></span>
                    <p class="fs3 center">Напишiть нам своє прохання або рекомендацiї, будемо дуже вдячнi за спiвпрацю!</p>
                </div>
            </div>

            <? echo do_shortcode('[contact-form-7 id="7" title="Контактна форма"]');?>

        </div>
    </section>
    <section class="l-bg" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="icons center">
                         <? if(have_rows('h_soc-links', $ID)):?>
                            <? while(have_rows('h_soc-links', $ID)): the_row();?>
                                <a href="<?the_sub_field('url');?>" class="icon-<?the_sub_field('class');?>"></a>
                            <? endwhile;?>
                        <? endif;?>
                    </div>
                    <p class="text-grey fs3 center">
                        <?the_field('copyright', $ID);?>
                    </p>
                    <ul class="footer-links center">
                        <li class="fs3 link-hov"><a href="/wp-admin">Вхід для співробітників</a></li>
                        <li class="fs3 link-hov"><a href="/карта-сайту">Карта сайту</a></li>
                    </ul>
                </div>
                <div class="col-sm-1">
                    <a href="#" style="display:none"><img class="block-center" src="<? echo get_template_directory_uri().'/assets/images/footer-logo.png';?>" alt=""></a>
                </div>
            </div>
        </div>
    </section>
    <span id="docwidth"></span>
    <? wp_footer();?>
</body>
</html>