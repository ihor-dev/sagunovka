$(document).ready(function(){
    
    //======Main page - submenu heigh fixer
    if($(window).width() > 531){  

        //====== Menu set items width    
        var count = 0;
        $(".submenu-items").each(function(a,b){
          count += 1;
        });

        $(".submenu-items").css("width",100/count +"%");      
        
        setTimeout(function(){
            var outH = 88;// $("#header_submenu").height();
            $(".submenu-items").css({"padding-top":"0", "padding-bottom":"0"});


            $(".submenu-items").each(function(a,b){
              if($(b).height() < outH){
                 var p = (outH-$(b).height())/2;
                $(b).css({'paddingTop':p, 'paddingBottom':p});
              }  
            }); 
        }, 1000);
    }

    //======Main page - tabs: hover
    $(".tabs.title").hover(
        function(){
            $(this).removeClass('g-bg').addClass('y-bg');
        },
        function(){
            if(!$(this).hasClass('active')){
                $(this).removeClass('y-bg').addClass('g-bg');
            }            
        }
    );
    
    //======Main page - tabs: show
    $(".tabs.title").click(function(){        
        var id = $(this).data('id');
        $(".tabs.title").removeClass('y-bg').removeClass('g-bg').removeClass('active').addClass('g-bg');
        $(this).removeClass('g-bg').addClass('y-bg').addClass('active');
        
        $(".tabs.content").removeClass('active');
        $("#"+id).slideDown(300, function(){
            $(this).addClass('active');
        });

    });


    //========Scroll fixed menu
    var $menu = $(".navbar");
         
    $(window).scroll(function(){
        if ( $(this).scrollTop() > 60 && !$menu.hasClass("fixed")){
            $menu.fadeOut('fast',function(){
                $(this).addClass("fixed")
                    .fadeIn('fast');
            });
        } else if($(this).scrollTop() <= 60 && $menu.hasClass("fixed")) {
            $menu.fadeOut(1,function(){
                $(this).removeClass("fixed")                    
                    .fadeIn(10);
            });
        }

    });    
    
    //==========debug----------
    /*
    $(window).resize(function(){
        var w = $('html').width();
        $("#docwidth").html(w);
    });
    */

    //=========SLICK SLIDER--------------
    if($(".slider-for").length>0){
        $(".slider-for").lightGallery({
            exThumbImage: 'data-exthumbimage'
        }); 


        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        
        $('.slider-for').on('afterChange', function(event, slick, currentSlide, nextSlide){
          $slide = $('div[data-slick-index='+currentSlide+']');
          
          $('.slider-for').animate({'height':$slide.height()}, 500);
        });

        var currentSlide = $('.slider-for').slick('slickCurrentSlide');
        $slide = $('div[data-slick-index='+currentSlide+']');
          
        $('.slider-for').animate({'height':$slide.height()}, 500);


        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: true,
            centerMode: false,
            focusOnSelect: true,
              responsive: [
                {
                  breakpoint: 543,
                  settings: {
                    dots: false
                  }
                }
            ]
        });

        
    }






    /* ======== Склад ради ================*/
    if($(".member-item").length > 0){
        var $grid =  $('.members').isotope({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.member-item',
            percentPosition: true,
            //layoutMode: 'fitRows',
        });
    }

    




    //scroll animation effects
    var $animation_elements = $('.animation-element');
    var $window = $(window);

    function check_if_in_view() {    
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');


})
