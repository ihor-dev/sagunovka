<? // TEMPLATE Новини - запис ?>
<? get_header();?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                        
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="fs1 b center"><? the_title();?></p>
                            </div>
                        </div>
                        <? if(count(get_field("images"))>0):?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="slider-for block-center">
                                     <? $imgs = get_field("images");  foreach($imgs as $img):?>
                                        <div data-src="<?=$img['url'];?>"><img src="<?=$img['url'];?>"></div>
                                    <? endforeach;?>
                                </div>                                
                                <div class="slider-nav l-bg">
                                     <? $imgs = get_field("images"); foreach($imgs as $img):?>                                     
                                        <div data-src="<?=$img['sizes']['medium'];?>" style="background:url(<?=$img['sizes']['medium'];?>) center center/cover;">
                                            
                                        </div>
                                    <? endforeach;?>
                                </div>
                            </div>
                        </div>

                        <? endif;?>  
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="line"></div>
                                <div class="fs2  nobmargin justify"><? the_content();?></div>            
                            </div>
                        </div>
                    <? endwhile;else:?>
                        <p class="fs1 b center">Ця сторінка знаходиться в стадії розробки</p>
                    <? endif;?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>