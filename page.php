<? get_header();?>

	<section id="content" class="">
       <div class="container">
            <div class="row">
                <div class="col-sm-12 block-center">                            
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                                                                            
                            <p class="fs1 b center"><? the_title();?></p>
                                
                                <? if(is_page(248)):?>
                                    <div id="container" class="museum"></div>
                                    <script>
                                        // create the panorama player with the container
                                        pano=new pano2vrPlayer("container");
                                        // add the skin object
                                        skin=new pano2vrSkin(pano);
                                        // load the configuration
                                        pano.readConfigUrl("http://sagunivka.ck.ua/3dmuseum/PANO_20160516_105725_out.xml");
                                        // hide the URL bar on the iPhone
                                        setTimeout(function() { hideUrlBar(); }, 10);
                                    </script>
                                <? endif;?>

                            <div class="fs2 nobmargin justify"><? the_content();?></div>
                    <? endwhile;else:?>
                        <p class="fs1 b center">Ця сторінка знаходиться в стадії розробки</p>
                    <? endif;?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>