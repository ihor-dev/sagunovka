<?
	/* Отключаем админ панель для всех пользователей. */
	show_admin_bar(false);


	//disable responsive images
	add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );

	//excerpt
	add_filter('excerpt_more', 'new_excerpt_more');
	function new_excerpt_more($more) {
		global $post;
		return '<a href="'. get_permalink($post->ID) . '">  Read more...</a>';
	}

	// новое меню
	register_nav_menus( array(
		'top'     => __( 'top', 'THEMENAME' )
	) );	

	$path = get_template_directory_uri();

	//return page id by page slug
	function get_id_by_slug($page_slug) {
		$page = get_page_by_path($page_slug);
		if ($page) {
			return $page->ID;
		} else {
			return null;
		}
	}


	function do_afterLogin() {
	    //do stuff
	    global $wpdb;
	    $sql = "UPDATE `wp_postmeta`,`wp_posts` 
				SET `post_status` = 'trash'
				WHERE `wp_posts`.`post_type`= 'announcements'						
				AND `wp_posts`.`ID` = `wp_postmeta`.`post_id`				
				AND `wp_postmeta`.`meta_key` = 'end_data'
				AND datediff(NOW(),`wp_postmeta`.`meta_value`) >= 0
                OR `wp_posts`.`post_type`= 'vacancies'
                AND `wp_posts`.`ID` = `wp_postmeta`.`post_id`				
				AND `wp_postmeta`.`meta_key` = 'end_data'
				AND datediff(NOW(),`wp_postmeta`.`meta_value`) >= 0";
	    $wpdb->query($sql);
	}
	add_action('wp_login', 'do_afterLogin');


	//add custom script to admin panel
	function my_enqueue( $hook ) {		
	    global $path;
	   	
	    wp_enqueue_script( 'adminjs', $path . '/assets/js/customadmin.js', array('jquery') );
	}

	add_action('admin_enqueue_scripts', 'my_enqueue');

	//post thumbnails
	add_theme_support( 'post-thumbnails' );
	
		//disable responsive images
	add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );

	
	// ====== Сайдбары ====== 
	function register_wp_sidebars() { 
		/* надпись в хедере */
		register_sidebar(
			
				array(
					'id' => 'footer_text',
					'name' => 'footer_sidebar', 
					'description' => 'BlogPress Releases', 
					'before_widget' => '', 
					'after_widget' => '',
					'before_title' => '<span>', 
					'after_title' => '</span>'
				)
				
		);
		register_sidebar(
				array(
					'id' => 'cookies_text',
					'name' => 'cookies_sidebar', 
					'description' => 'Top cookies message', 
					'before_widget' => '', 
					'after_widget' => '',
					'before_title' => '', 
					'after_title' => ''
				)
		);

	 
	}	
	add_action( 'widgets_init', 'register_wp_sidebars' );

	//render menu
	function get_menu($level = 1)
	{  
	    $menu_name = 'top';
	    $locations = get_nav_menu_locations();
	    
	    if( isset($locations[ $menu_name ]) )
	    {

	        $menuID = wp_get_nav_menu_object( $locations[ $menu_name ] ); // получаем ID
	        $menu_items = wp_get_nav_menu_items( $menuID ); // получаем элементы меню
	        
	        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	        	$currUrl = "https://". $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	        } else {
	        	$currUrl = "http://". $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	        }
	        
		    $out = parse_url($currUrl);
		    $exp = explode('/', $out['path']);
		    if($exp[1] !== ""){
		    	$end = "/";
		    }else {
		    	$end = "";
		    }
		    $currUrl = $out['scheme']."://".$out['host'].'/'.$exp[1].$end;
		    
	        
	        foreach((array) $menu_items as $key => $val){
	            if($val->menu_item_parent == 0){   
	                if($val->url == $currUrl){
	                    $currUrlID = $val->ID;
	                }         
	                $menu[$val->ID] = $val;
	            }else{
	            	
	            	if($val->url == $currUrl){

	            		$currUrlID = $val->menu_item_parent;
	            		$currSubUrlID = $val->ID;
	            	}
	                $submenu[$val->menu_item_parent][] = $val;
	            }     
	        }


	        // создаем список
	        $menu_list = '';

	        if($level == 1){
	            foreach ( (array) $menu as $key => $menu_item ){
	                $active = $key == $currUrlID ? " active" : "";
	                $menu_list .= '<li class="nav-item'.$active.' w-hov"><a class="nav-link" href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
	            } 
	        }elseif($level == 2){//home page submenu
	            foreach ( (array) $submenu[$currUrlID] as $key => $menu_item ){ 
	            	$active = $menu_item->ID == $currSubUrlID ? " active" : "";               
	                $menu_list .= '<div class="submenu-items center y-hov'.$active.'"><a  class="fs2 b" href="' . $menu_item->url . '">' . $menu_item->title . '</a></div>';
	            } 
	        }else{//other pages submenu

	            foreach ( (array) $submenu[$currUrlID] as $key => $menu_item ){	            	
	            	$active = $menu_item->ID == $currSubUrlID ? " active" : "";
	                $menu_list .= '<div class="submenu-items center y-hov'.$active.'"><a  class="fs2 b" href="' . $menu_item->url . '">' . $menu_item->title . '</a></div>';
	            } 
	        }  

	        echo $menu_list;
	    }
	}

	add_filter('the_content', 'wpautop');

	function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	/*
	function show_template() {
	      global $template;
	      print_r($template);
	}
	add_action('wp_footer', 'show_template');
	/*
?>