<? // TEMPLATE  Вакансії ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'vacancies',                                 
        'posts_per_page' => 8,
        'paged' => get_query_var('paged')     
    );
    $wp = new WP_Query( $args );  
?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( $wp->have_posts() ) : while ( $wp->have_posts() ) : $wp->the_post(); ?>
                        <a href="<? the_permalink();?>" class="vacancy-item center">                            
                            <p class="fs2 b nobmargin center"><? the_title();?></p>
                            <div class="line block-center"></div>
                            <p class="fs3 nobmargin justify"><?= strip_tags(mb_substr(str_replace(array('<p>','</p>'), array('',''),get_the_content()), 0,528)); ?>...</p>
                        </a>

                        
                    <? endwhile;?>
                <? else:?>
                    <p class="fs1 text-center">На даний момент записи вiдсутнi</p>
                <? endif;?>
                    <? wp_pagenavi(array( 'query' => $wp ) );?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>