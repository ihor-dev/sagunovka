<? // TEMPLATE NAME: Склад рады ?>
<? get_header();?>
    <section>
        <div class="container-inner">
            <div class="row">
                <div class="col-sm-12">
                    <p class="fs1 center"><?the_field('h_members_title');?></p>
                    <span class="line"></span>
                    <p class="fs3 center text-grey"><?the_field('h_members_subtitle');?></p>
                </div>
            </div>
            <div class="row members">
                <div class="col-sm-12">
            	<? if(have_rows('h_member')):?>
            		<? while(have_rows('h_member')): the_row()?>
						<div class="member-item">
		                    <div style="background-image:url(<?the_sub_field('img');?>)" class="rounded y-border"></div>
		                    <p class="fs2 b center"><?the_sub_field('member-name');?></p>
		                    <p class="fs3 g-text center"><?the_sub_field('job');?></p>
		                </div>
            		<? endwhile;?>
            	<? endif;?>
                </div>
            </div>
        </div>
    </section>
    <? get_footer();?>