<? // Сессії ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'session',                                 
        'posts_per_page' => 4,
        'paged' => get_query_var('paged')     
    );
    $docs = new WP_Query( $args );  
?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-10 col-lg-11 block-center">                            
                    <?php if ( $docs->have_posts() ) : while ( $docs->have_posts() ) : $docs->the_post(); ?>                        
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="<?the_permalink();?>" class="info-item center">
                                        <? $img = '';
                                            $img = get_the_post_thumbnail($post->id);
                                            if($img != ''){
                                                echo $img;
                                            }else {
                                                echo '<img src="'.get_template_directory_uri().'/assets/images/session.jpg">';
                                            }
                                        ?>
                                        <span class="date fs3 center">Сессія від <? echo get_the_date();?></span>
                                    </a>
                                    
                                </div>
                                <div class="col-sm-8">
                                    <a href="<?the_permalink();?>" class="info-item justify">
                                        <p class="fs2 nobmargin"><? the_title();?></p>
                                        <p class="fs3 nobmargin"><?=str_replace(array('<p>','</p>'), array('',''),get_the_excerpt());?></p>
                                    </a>
                                </div>
                            </div>                                                    

                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $docs ) );?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>