<? // TEMPLATE Новини світу ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'global-news',                                 
        'posts_per_page' => 20,
        'paged' => get_query_var('paged')     
    );
    $docs = new WP_Query( $args );  
?>


    <section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-10 col-lg-11 block-center">                            
                    <?php if ( $docs->have_posts() ) : while ( $docs->have_posts() ) : $docs->the_post(); ?>                                                                                
                        <a href="<?the_permalink();?>" class="worldnews-item center">
                            <? echo get_the_post_thumbnail($post->id);?>
                            <p class="fs3 center"><? the_title();?></p>
                        </a>                                
                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $docs ) );?>
                    <? wp_reset_query();?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>