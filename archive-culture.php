<? // TEMPLATE  Культура ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'culture',                                 
        'posts_per_page' => 3,
        'paged' => get_query_var('paged')     
    );
    $docs = new WP_Query( $args );  
?>


	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-11 block-center">                            
                    <?php if ( $docs->have_posts() ) : while ( $docs->have_posts() ) : $docs->the_post(); ?>                    
                            <div class="row sport">
                                <div class="col-sm-5">
                                    <a href="<?the_permalink();?>" class="info-item center">
                                        <? echo get_the_post_thumbnail($post->id);?>                                        
                                    </a>
                                </div>
                                <div class="col-sm-7">
                                    <a href="<?the_permalink();?>" class="info-item center">
                                        <p class="fs2 nobmargin b"><? the_title();?></p>
                                        <p class="fs3 nobmargin"><?= strip_tags(mb_substr(str_replace(array('<p>','</p>'), array('',''),get_the_content()), 0,528)); ?>...</p>
                                    </a>
                                </div>
                            </div>                                                    

                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $docs ) );?>
                    <? wp_reset_query();?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>