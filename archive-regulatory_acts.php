<? //Регуляторні акти?>
<? get_header();?>

<?  
    $url = $_SERVER['REQUEST_URI'];
    $url = str_replace(strpbrk($url, '?'), "", $url);
    $cl1 = $cl2 = '';
    if(isset($_GET['filter']) && $_GET['filter'] == 'archive' ) {
        $filter = 'Архiв';
        $cl2 = 'filter-underline';
    }else{
        $filter = "Активно";
        $cl1 = 'filter-underline';
    }

    $args = array(
        'post_type' => 'regulatory_acts',                                 
        'posts_per_page' => 8,
        'paged' => get_query_var('paged'),
        'meta_key'      => 'curr-status',
        'meta_value'    => $filter   
    );
    $wp = new WP_Query( $args );  
?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-11 block-center right">                    
                    <a href="<? echo $url;?>?filter=current" class="sort fs2 link-hov <?=$cl1;?>">Активнi</a><span class="fs2 text-shadow text-yellow"> | </span><a href="<? echo $url;?>?filter=archive" class="sort fs2 link-hov <?=$cl2;?>">Архiвнi</a>
                    <br><br>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( $wp->have_posts() ) : while ( $wp->have_posts() ) : $wp->the_post(); ?>
                        <a href="<? the_permalink();?>" class="vacancy-item center">
                            <? 
                                $st = get_field('status') == '1' ? 'Затверджено' : "Не затверджено";                                                            
                            ?>
                            <p class="fs2 b nobmargin center"><?the_field('title');?> <i>(<?=$st;?>)</i></p>
                            <div class="line block-center"></div>
                            <p class="fs3 nobmargin center"><? the_title();?></p>
                        </a>

                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $wp ) );?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>