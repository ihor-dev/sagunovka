<? // TEMPLATE Галерея - запис ?>
<? get_header();?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                        
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="fs1 b center"><? the_title();?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">                                
                                <div class="fs2  nobmargin justify"><? the_content();?></div>            
                            </div>
                        </div>
                    <? endwhile;else:?>
                        <p class="fs1 b center">Ця сторінка знаходиться в стадії розробки</p>
                    <? endif;?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>