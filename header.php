<? global $path; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Сагунiвська Сiльська Рада<? wp_title(); ?></title>
    <link rel="stylesheet" href="<?=$path;?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$path;?>/style.css">
    <link rel="stylesheet" href="<?=$path;?>/assets/css/media-queries.css">
    <link rel="stylesheet" href="<?=$path;?>/assets/css/animation.css">
    <link rel="stylesheet" href="<?=$path;?>/assets/css/fontello.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css"/>

    <link rel="stylesheet" href="<?=$path;?>/assets/css/lightgallery.css"/>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="<?=$path;?>/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
    <script src="<?=$path;?>/assets/js/lightgallery-all.min.js"></script>
    <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>
    
    <script src="<?=$path;?>/assets/js/main.js"></script>

    <? if(is_page(248)):?>
        <script type="text/javascript" src="<?=$path;?>/assets/js/museum.js"></script>
        <script type="text/javascript" src="/3dmuseum/pano2vr_player.js"></script>
        <script type="text/javascript" src="/3dmuseum/skin.js"></script>
        <script type="text/javascript" src="/3dmuseum/skin.js"></script>



        
    
    <? endif;?>
    

    <?php wp_head(); ?> 
</head>
<body>

    <nav class="navbar navbar-light navbar-static-top bg-faded y-bg">
        <div class="container-inner">
            <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#topMenu">
            &#9776;
            </button>
            <div class="collapse navbar-toggleable-sm" id="topMenu">            
                <ul class="nav navbar-nav">
                    <? get_menu(1);?>
                </ul>
            </div>
        </div>
        <a href="<?=get_home_url();?>"><img class="logo" style="background-color: #FFD200;" src="<?=$path;?>/assets/images/gerb.png" alt="logo"></a>
    </nav>

    <header>
        <div class="container-inner">
            <div class="row">
                <div class="col-sm-12">
                    <?
                    
                        global $wp_query;   
                        //var_dump($wp_query->query['post_type']);
                        if(is_page('gallery')){
                            echo do_shortcode('[rev_slider alias="gallery"]');
                        }
                        else if(isset($wp_query->query['post_type']) && $wp_query->query['post_type'] == "sport"){
                            echo do_shortcode('[rev_slider alias="sport"]');
                        }
                        else if(isset($wp_query->query['post_type']) && $wp_query->query['post_type'] == "culture"){
                            echo do_shortcode('[rev_slider alias="culture"]');
                        }                    
                        else if(is_front_page()){
                            echo do_shortcode('[rev_slider alias="home-slider"]');
                        }
                        else{
                            echo do_shortcode('[rev_slider alias="other"]');
                        }
                    ?>
    
                </div>
            </div>
        </div>        
        <? if(is_front_page()):?>
            <div class="container-inner home" id="header_submenu">
                <div class="row">
                    <div class="col-sm-12 block-center">
                        <div class="submenu-items">
                        <p class="fs2 b center nobmargin">Останнi Новини</p>
                        <p class="fs3 text-grey center nobmargin">Найсвiжiшi новини села</p>
                    </div>
                        <? get_menu(2);?>
                    </div>
                </div>
            </div>
        <? else: ?>
            <div class="container-inner other" id="header_submenu">
                <div class="row">
                    <div class="col-sm-12 block-center">
                        <? get_menu(22);?>
                    </div>
                </div>
            </div>
        <? endif;?>
        
    </header>