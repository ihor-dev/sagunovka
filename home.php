<? // TEMPLATE NAME: Головна ?>
<? get_header();?>
	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-3">                   
                    <p class="fs3 b tabs title center y-bg active" data-id="tab1"><span class="triangle"></span><?the_field('h_info_title1');?></p>
                    <p class="fs3 b tabs title center g-bg" data-id="tab2"><span class="triangle"></span><?the_field('h_info_title2');?></p>
                    <p class="fs3 b tabs title center g-bg" data-id="tab3"><span class="triangle"></span><?the_field('h_info_title3');?></p>
                </div>
                <div class="col-sm-9">
                    <div class="tabs content active" id="tab1">
                    	<p class="fs2 b"><?the_field('h_info_title1');?></p>
                        <p class="fs3"><?the_field('h_info_content1');?></p>
                    </div>
                    <div class="tabs content" id="tab2">
                    	<p class="fs2 b"><?the_field('h_info_title2');?></p>
                        <p class="fs3"><?the_field('h_info_content2');?></p>
                    </div>
                    <div class="tabs content" id="tab3">
                    	<p class="fs2 b"><?the_field('h_info_title3');?></p>
                        <p class="fs3"><?the_field('h_info_content3');?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="fs1 center"><?the_field('h_members_title');?></p>
                    <span class="line"></span>
                    <p class="fs3 center text-grey"><?the_field('h_members_subtitle');?></p>
                </div>
            </div>
            <div class="row members">
            	<? if(have_rows('h_member')):?>
            		<? $i=0; while(have_rows('h_member') && $i < 3): the_row(); $i++;?>
                        <? if($i==1){
                            $animclass='slide-left';
                        }else if($i==2){
                            $animclass='slide-up';
                        }else if($i==3){
                            $animclass='slide-right';                            
                        }                            
                        ?>
                        <div class="member-item animation-element <?=$animclass;?>">
                            <div style="background-image:url(<?the_sub_field('img');?>)" class="rounded y-border"></div>
                            <p class="fs2 b center"><? the_sub_field('member-name');?></p>
                            <p class="fs3 g-text center"><?the_sub_field('job');?></p>
                        </div>                        
            		<? endwhile;?>
            	<? endif;?>
            </div>
        </div>
    </section>
    <? get_footer();?>
   