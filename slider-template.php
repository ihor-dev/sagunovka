<? if(count(get_field("images"))>0):?>
    <div class="row">
        <div class="col-sm-12">
            <div class="slider-for block-center">
                 <? $imgs = get_field("images");  foreach($imgs as $img):?>
                    <div data-src="<?=$img['url'];?>" data-exthumbimage="<?=$img['sizes']['medium'];?>" style="background:url(<?=$img['url'];?>) center center/cover;"></div>
                <? endforeach;?>
            </div>                                
            <div class="slider-nav l-bg">
                 <? $imgs = get_field("images"); foreach($imgs as $img):?>                                     
                    <div data-src="<?=$img['sizes']['medium'];?>" style="background:url(<?=$img['sizes']['medium'];?>) center center/cover;">
                        
                    </div>
                <? endforeach;?>
            </div>
        </div>
    </div>

<? endif;?>  