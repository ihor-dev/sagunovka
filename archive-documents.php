<? // TEMPLATE  Документи ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'documents',                                 
        'posts_per_page' => 12,
        'paged' => get_query_var('paged')     
    );
    $docs = new WP_Query( $args );  
?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-12">                            
                    <?php if ( $docs->have_posts() ) : while ( $docs->have_posts() ) : $docs->the_post(); ?>
                        <a href="<?the_field('document');?>" class="docs-item center">
                            <img src="<?=get_template_directory_uri();?>/assets/images/doc-img.png" alt="">
                            <p class="fs2 b nobmargin center"><? the_title();?></p>
                            <p class="fs3 b nobmargin center"><?the_field('doc_descr');?></p>
                        </a>

                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $docs ) );?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>