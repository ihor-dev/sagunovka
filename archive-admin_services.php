<? // TEMPLATE  Адміністративні послуги ?>
<? get_header();?>

<?
    $args = array(
        'post_type' => 'admin_services',                                 
        'posts_per_page' => 24,
        'paged' => get_query_var('paged')     
    );
    $wp = new WP_Query( $args );  
?>

	<section id="content" class="l-bg">
       <div class="container-inner">
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( $wp->have_posts() ) : while ( $wp->have_posts() ) : $wp->the_post(); ?>
                        <a href="<?the_permalink();?>" class="announcement-item">                            
                            <p class="fs2 b nobmargin center"><? the_title();?></p>                            
                            <div class="line"></div>
                            <p class="fs3 nobmargin justify"><?= strip_tags(mb_substr(str_replace(array('<p>','</p>'), array('',''),get_the_content()), 0,528)); ?>...</p>
                        </a>

                        
                    <? endwhile; endif;?>
                    <? wp_pagenavi(array( 'query' => $wp ) );?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>