<? // TEMPLATE  Оголошення - запис ?>
<? get_header();?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-12 block-center">                            
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                        
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-xs-3 col-lg-1">
                                <img src="<?=get_template_directory_uri();?>/assets/images/ogoloshennya-img.png" alt="icon">
                            </div>
                            <div class="col-sm-10 col-md-10 col-xs-9 col-lg-11">
                                <p class="fs1 b left"><? the_title();?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <i class="icon-calendar"></i><? the_date();?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="line"></div>
                                <div class="fs2 nobmargin justify"><? the_content();?></div>
                            </div>
                        </div>
                    <? endwhile;else:?>
                        <p class="fs1 b center">Ця сторінка знаходиться в стадії розробки</p>
                    <? endif;?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>