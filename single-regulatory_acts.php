<? // Регуляторний акт - запис ?>
<? get_header();?>

	<section id="content" class="l-bg">
       <div class="container">
            <div class="row">
                <div class="col-sm-11 block-center">                            
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                        
                        <div class="row">
                            <div class="col-sm-12">
                                <?
                                    $st = get_field('status') == '1' ? 'Затверджено' : "Не затверджено";                                                            
                                ?>
                                <p class="fs1 b left"><?the_field('title');?>: "<? the_title();?>" (<?=$st;?>)</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <i class="icon-calendar"></i><? the_date();?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="line"></div>                            
                                <div class="fs2  nobmargin justify"><? the_content();?></div>
                            </div>
                        </div>
                    <? endwhile;else:?>
                        <p class="fs1 b center">Ця сторінка знаходиться в стадії розробки</p>
                    <? endif;?>
                </div>
            </div>
        </div>
    </section>

<? get_footer();?>